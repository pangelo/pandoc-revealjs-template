
include config.mk

all: $(OUTPUT)

$(OUTPUT): $(INPUT_FILE) $(LOCAL_CSS) $(EXTRA_CONFIG)
	pandoc -f $(INPUT_FORMAT) -t revealjs -s -V revealjs-url=$(REVEAL_PATH) -V theme=$(REVEAL_THEME) -c $(LOCAL_CSS) --slide-level $(SLIDE_LEVEL) -A $(EXTRA_CONFIG) $(INPUT_FILE) -o $(OUTPUT)

preview: $(OUTPUT)
	xdg-open file://$(PWD)/presentation.html

help:
	@echo Available targets
	@echo   make          (build presentation)
	@echo   make preview  (launch presentation in browser)
	@echo   make clean    (clean up stuff)
	@echo   make help     (this help text, how meta)

clean:
	@echo "nothing happens"

.PHONY: preview help clean
