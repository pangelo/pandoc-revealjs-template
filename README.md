Simple base template to author presentations using Pandoc and Reveal.js
=======================================================================

This package provides a base folder structure and Makefile that can
serve as a base starting point to author presentations using Pandoc
and Reveal.js

This assumes a Linux environment, and should probably work on OSX.
Patches welcome to help support other environments.

To use it you will need GNU Make, Pandoc and Reveal.js and then fiddle
with the variables in config.mk to setup your presentation build.

If you want you can pull Reveal.js as a git submodule using:

    git submodule init
    git submodule update

And then build the library:

    cd reveal.js
    npm install
    grunt
