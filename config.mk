#
# Build Configuration
#

# Name of the file with the presentation content
INPUT_FILE = presentation.md

# Format of the input file
INPUT_FORMAT = markdown

# Name of the generated output file
OUTPUT = presentation.html

# Reveal.js presentation theme 
# One of: beige, black, blood, league, moon, night, serif, simple, 
# sky, solarized, white
REVEAL_THEME = night

# Slide level (levels above are used for title slides, levels 
# below are used for slide content)
SLIDE_LEVEL = 2

# CSS file with local theme customizations
LOCAL_CSS = local.css

# File containing additional Reveal.js configuration
EXTRA_CONFIG = config.html

# Path to Reveal.js
REVEAL_PATH = reveal.js
